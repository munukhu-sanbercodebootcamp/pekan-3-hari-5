@extends('adminlte.master')

@section('content')

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Edit Pertanyaan ID ke-{{$posts->id}}</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" action="/pertanyaan/{pertanyaan_id}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Judul</label>
                                <input type="text" class="form-control" id="judul" name="judul" value="{{old('judul',$posts->judul)}}" placeholder="Judul Pertanyaan">
                                @error('judul')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Isi</label>
                                <input type="text" class="form-control" id="isi" name="isi" value="{{old('isi',$posts->isi)}}" placeholder="Isi">
                                @error('isi')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Tanggal Dibuat</label>
                                <input type="date" class="form-control" id="tanggal_dibuat" name="tanggal_dibuat" value="{{old('tanggal_dibuat',$posts->tanggal_dibuat)}}" placeholder="Dibuat">
                                @error('tanggal_dibuat')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Tanggal Diperbaharui</label>
                                <input type="date" class="form-control" id="tanggal_diperbaharui" name="tanggal_diperbaharui" value="{{old('tanggal_diperbaharui',$posts->tanggal_diperbaharui)}}" placeholder="Diperbaharui">
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>

@endsection
