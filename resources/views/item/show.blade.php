@extends('adminlte.master')

@section('content')
<div class="card-body">
<a href="/pertanyaan" type="button" class="btn btn-primary mb-2">Back</a>
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>Judul</th>
          <th>Isi</th>
          <th>Tanggal Dibuat</th>
          <th>Tanggal Diperbaharui</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>{{$posts->judul}}</td>
          <td>{{$posts->isi}}</td>
          <td>{{$posts->tanggal_dibuat}}</td>
          <td>{{$posts->tanggal_diperbaharui}}</td>
        </tr>
      </tbody>
    </table>
  </div>

@endsection
