@extends('adminlte.master')

@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">List Pertanyaan</h3>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    @if(session('success'))
    <div class="alert alert-success">
      {{session('success')}}
    </div>
    @endif
    <table class="table table-bordered">
      <a href="/pertanyaan/create" type="button" class="btn btn-primary mb-2">Create</a>
      <thead>
        <tr>
          <th style="width: 10px">#</th>
          <th>Judul</th>
          <th>Isi</th>
          <th style="width: 60px">Action</th>
        </tr>
      </thead>
      <tbody>
        @forelse($posts as $key => $value)
        <tr>
          <td>{{$key + 1}}</td>
          <td>{{$value->judul}}</td>
          <td>{{$value->isi}}</td>
          <td style="display:flex">
            <a href="/pertanyaan/{{$value->id}}" type="button" class="btn btn-info btn-sm">Show</a>
            <a href="/pertanyaan/{{$value->id}}/edit" type="button" class="btn btn-warning btn-sm">Edit</a>
            <form action="/pertanyaan/{{$value->id}}" method="post">
              @csrf
              @method("DELETE")
              <input type="submit" class="btn btn-danger btn-sm" value="Delete">
            </form>
          </td>
        </tr>
        @empty
        <tr>
          <td colspam="4" align="center">No Post</td>
        </tr>
        @endforelse
      </tbody>
    </table>
  </div>

</div>
@endsection
