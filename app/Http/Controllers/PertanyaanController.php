<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PertanyaanController extends Controller
{
    public function index(Request $request){
        $posts = DB::table('pertanyaan')->get();
        return view('item.tableCRUD', compact('posts'));
    }

    public function create(Request $request){
        return view('item.create');
    }

    public function store(Request $request){
        $request->validate([
            'judul' => 'required|max:45',
            'isi' => 'required|max:255',
            'tanggal_dibuat' => 'required'
        ]);
        $query = DB::table('pertanyaan')->insert(
            ['judul' => $request['judul'], 'isi' => $request['isi'],
            'tanggal_dibuat' => $request['tanggal_dibuat'],'tanggal_diperbaharui' => $request['tanggal_diperbaharui']]
        );
        return redirect('/pertanyaan')->with('success','Pertanyaan Baru Berhasil Dibuat!');
    }

    public function show($id){
        $posts = DB::table('pertanyaan')->where('id',$id)->first();
        return view('item.show', compact('posts'));
    }

    public function edit($id){
        $posts = DB::table('pertanyaan')->where('id',$id)->first();
        return view('item.edit', compact('posts'));
    }

    public function update($id, Request $request){
        $request->validate([
            'judul' => 'required|max:45',
            'isi' => 'required|max:255',
            'tanggal_dibuat' => 'required',
            'tanggal_diperbaharui' => 'required'
        ]);
        $query = DB::table('pertanyaan')
        ->where('id',$id)
        ->update([
            'judul'=>$request['judul'],
            'isi'=>$request['isi'],
            'tanggal_dibuat'=>$request['tanggal_dibuat'],
            'tanggal_diperbaharui'=>$request['tanggal_diperbaharui']
        ]);
        return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Di Update!');
    }

    public function destroy($id){
        $query = DB::table('pertanyaan')->where('id', $id)->delete();

        return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Di Hapus!');
    }
}
